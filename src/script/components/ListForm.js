export class ListForm {
    constructor() {
        this.list = [];
        this.item = "";

        this.selectors();
        this.events();
    }

    selectors() {
        this.input = document.querySelector('input[type="file"]');
        this.form = document.querySelector(".list-form");
        this.nameInput = document.querySelector(".list-form-input-item-name");
        this.descriptionInput = document.querySelector(".list-form-input-item-description");
        this.previewImage = document.querySelector(".list-form-image-wrapper");
        this.items = document.querySelector(".list-result-wrapper");
        this.label = document.querySelector(".list-form-label-image");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
        this.input.addEventListener("change", this.previewImageInput.bind(this));
    }

    previewImageInput(event) {
        event.preventDefault();
        const reader = new FileReader();
        reader.onload = () => {
            this.item = reader.result;
            this.previewImage.style = "background-image: url('" + this.item + "');";
        };
        reader.readAsDataURL(event.target.files[0]);
        this.label.innerHTML = "";
    }

    addItemToList(event) {
        event.preventDefault();
        const itemImage = this.item;
        const itemName = event.target["item-name"].value;
        const itemDescription = event.target["item-description"].value;

        if (itemName != "" && itemDescription != "" && itemImage != "") {
            let stringDescription = itemDescription;
            let stringResultDescription = "";
            for (let i = 0; i < stringDescription.length; i++) {
                if (i % 30 == 0 && i != 0) {
                    stringResultDescription =
                        stringResultDescription + stringDescription[i] + "<br>";
                } else {
                    stringResultDescription = stringResultDescription + stringDescription[i];
                }
            }

            let stringName = itemName;
            let stringResultName = "";
            for (let i = 0; i < stringName.length; i++) {
                if (i % 15 == 0 && i != 0) {
                    stringResultName = stringResultName + stringName[i] + "<br>";
                } else {
                    stringResultName = stringResultName + stringName[i];
                }
            }

            const itemsObj = {
                name: stringResultName,
                description: stringResultDescription,
                image: itemImage,
            };
            this.list.push(itemsObj);
            this.renderListItems();
            this.resetInputs();
        }
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach(function (itemsObj) {
            itemsStructure += `
                <div class="list-result">
                    <img class="list-result-image" src="${itemsObj.image}" />
                    <div class="list-result-text-wrapper">
                        <h4 class="list-result-name">${itemsObj.name}</h4>
                        <p class="list-result-description">${itemsObj.description}</p>
                    </div>
                </div>
            `;
        });

        this.items.innerHTML = itemsStructure;
    }

    resetInputs() {
        this.nameInput.value = "";
        this.descriptionInput.value = "";
        this.previewImage.style = "";
        this.label.innerHTML = "imagem";
        this.item = "";
    }
}
